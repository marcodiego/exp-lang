CC      := gcc
CCFLAGS :=
LDFLAGS :=

TARGETS:= exp-lang
MAINS  := $(addsuffix .o, $(TARGETS) )
OBJ    := mpc/mpc.o $(MAINS)
DEPS   := mpc/mpc.h

.PHONY: all clean

all: $(TARGETS)

clean:
	rm -f $(TARGETS) $(OBJ)

$(OBJ): %.o : %.c $(DEPS)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(TARGETS): % : $(filter-out $(MAINS), $(OBJ)) %.o
	$(CC) -o $@ $(LIBS) $^ $(CCFLAGS) $(LDFLAGS)