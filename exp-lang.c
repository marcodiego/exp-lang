/* SPDX-License-Identifier:	GPL-3.0-or-later */

#include <stdio.h>
#include "mpc/mpc.h"

struct param {
	char *name;
	int value;
};

struct param *eval_stmt(mpc_ast_t *node, struct param *paramlist, int *retval);

struct param *get_param(struct param *paramlist, char *param_name, size_t *retval)
{
	size_t pos = 0;

	// First we'll try to find the parameter (or variable?) we want to assign
	while(paramlist[pos].name != NULL){
		if(strcmp(paramlist[pos].name, param_name) == 0)
			break;
		pos++;
	}

	// if we can't find it, we'll add it to our list
	if(paramlist[pos].name == NULL) {
		paramlist = realloc(paramlist, (pos+1)*sizeof(struct param));
		paramlist[pos+1].name = NULL;

		paramlist[pos].name = param_name;
		paramlist[pos].value = 0;
	}

	*retval = pos;
	return paramlist;
}

struct param *eval_id(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	size_t pos;
	paramlist = get_param(paramlist, node->contents, &pos);
	*retval = paramlist[pos].value;

	return paramlist;
}

struct param *eval_message(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	size_t len = strlen(node->contents);
	if(node->contents[0] =='\'') {
		node->contents[len-1] = '\0';
		*retval = printf("%s", &(node->contents[1]));
		node->contents[len-1] = '\'';
	}else{
		node->contents[len-1] = '\0';
		*retval = printf("%s\n", &(node->contents[1]));
		node->contents[len-1] = '\"';
	}

	return paramlist;
}

struct param *eval_value(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "id"))
		return eval_id(node, paramlist, retval);

	if(strstr(node->tag, "message"))
		return eval_message(node, paramlist, retval);

	if(node->children_num == 3)
		return eval_stmt(node->children[1], paramlist, retval);

	if(node->children_num == 2) {
		paramlist = eval_value(node->children[1], paramlist, retval);
		switch(node->children[0]->contents[0]) {
			case '-':
				*retval = -(*retval);
			break;
			case '!':
				*retval = !(*retval);
			break;
			case '#':
				*retval = printf("%d", *retval);
			break;
			case '$':
				*retval = printf("%d\n", *retval);
			break;
			default:
			break;
		}
		return paramlist;
	}

	// @ denotes an input
	if(strcmp(node->contents, "@") == 0){
		scanf("%d", retval);
	} else {
		*retval = atoi(node->contents);
	}

	return paramlist;
}

struct param *eval_rdiv(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "value"))
		return eval_value(node, paramlist, retval);

	if(node->children_num == 1)
		return eval_value(node->children[0], paramlist, retval);

	int val1, val2;
	paramlist = eval_value(node->children[0], paramlist, &val1);
	paramlist = eval_rdiv(node->children[2], paramlist, &val2);

	*retval = val1%val2;

	return paramlist;
}

struct param *eval_prods(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "rdiv"))
		return eval_rdiv(node, paramlist, retval);

	char op = node->children[1]->contents[0];

	int val1, val2;
	paramlist = eval_rdiv(node->children[0], paramlist, &val1);
	paramlist = eval_prods(node->children[2], paramlist, &val2);

	*retval = (op == '*') ? val1*val2 : val1/val2;

	return paramlist;
}

struct param *eval_sums(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "prods"))
		return eval_prods(node, paramlist, retval);

	if(node->children_num == 1)
		return eval_prods(node->children[0], paramlist, retval);

	char op = node->children[1]->contents[0];

	int val1, val2;
	paramlist = eval_prods(node->children[0], paramlist, &val1);
	paramlist = eval_sums(node->children[2], paramlist, &val2);

	*retval = (op == '+') ? val1+val2 : val1-val2;

	return paramlist;
}

struct param *eval_comps(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "sums"))
		return eval_sums(node, paramlist, retval);

	if(node->children_num == 1)
		return eval_sums(node->children[0], paramlist, retval);

	char *op = node->children[1]->contents;

	int val1, val2;
	paramlist = eval_sums(node->children[0], paramlist, &val1);
	paramlist = eval_comps(node->children[2], paramlist, &val2);

	*retval = (strcmp(op, ">") == 0) ? val1 > val2 :
			  (strcmp(op, "<") == 0) ? val1 < val2 :
			  (strcmp(op, "==") == 0) ? val1 == val2 :
			  (strcmp(op, "!=") == 0) ? val1 != val2 :
			  (strcmp(op, ">=") == 0) ? val1 >= val2 :
			  (strcmp(op, "<=") == 0) ? val1 <= val2 :
			  0;

	return paramlist;
}

struct param *eval_logics(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "comps"))
		return eval_comps(node, paramlist, retval);

	if(node->children_num == 1)
		return eval_comps(node->children[0], paramlist, retval);

	char *op = node->children[1]->contents;

	int val1, val2;
	paramlist = eval_comps(node->children[0], paramlist, &val1);
	paramlist = eval_logics(node->children[2], paramlist, &val2);

	*retval = (strcmp(op, "||") == 0) ? val1 || val2 :
		  	  (strcmp(op, "&&") == 0) ? val1 && val2 :
		  	  0;

	return paramlist;
}

struct param *eval_cond(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "logics"))
		return eval_logics(node, paramlist, retval);

	if(node->children_num == 1)
		return eval_logics(node->children[0], paramlist, retval);

	paramlist = eval_logics(node->children[0], paramlist, retval);

	struct param *ret = *retval					? eval_stmt(node->children[2], paramlist, retval):
						node->children_num == 5 ? eval_stmt(node->children[4], paramlist, retval)
												: 0;

	return ret;
}

struct param *process_assignm(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "cond"))
		return eval_cond(node, paramlist, retval);

	size_t pos = 0;
	paramlist = get_param(paramlist, node->children[0]->contents, &pos);
	paramlist = eval_cond(node->children[2], paramlist, retval);
	paramlist[pos].value = *retval;

	return paramlist;
}

struct param *eval_loop(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	int cond;
	*retval = 0;
	if(node->children[0]->contents[0] == '[') {
		//This is a "while" loop
		for(;;) {
			paramlist = eval_stmt(node->children[1], paramlist, &cond);
			if(!cond)
				break;
			paramlist = eval_stmt(node->children[3], paramlist, retval);
		}
	}else{
		//This is a do..."while" loop
		for(;;) {
			paramlist = eval_stmt(node->children[0], paramlist, retval);
			paramlist = eval_stmt(node->children[2], paramlist, &cond);
			if(!cond)
				break;
		}
	}

	return paramlist;
}

struct param *process_stmt_list(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	int pos;

	for(pos = 0; pos < node->children_num; pos += 2)
		paramlist = eval_stmt(node->children[pos], paramlist, retval);

	return paramlist;
}

struct param *eval_body(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	*retval = 0;
	if(node->children_num == 2)
		return paramlist;
	return process_stmt_list(node->children[1], paramlist, retval);
}

struct param *eval_stmt(mpc_ast_t *node, struct param *paramlist, int *retval)
{
	if(strstr(node->tag, "loop"))
		return eval_loop(node, paramlist, retval);

	if(strstr(node->tag, "body"))
		return eval_body(node, paramlist, retval);

	if(strstr(node->tag, "assign"))
		return process_assignm(node, paramlist, retval);

	if(strstr(node->tag, "cond"))
		return eval_cond(node, paramlist, retval);

	if(strstr(node->tag, "logics"))
		return eval_logics(node, paramlist, retval);


	fprintf(stderr, "This should never have happened!\n");

	return 0;//should never happen
}

int main(int argc, char *argv[])
{
	if(argc < 2) {
		printf("Usage: %s filename\n", argv[0]);
		exit(1);
	}

	char *filename = argv[1];

	mpc_parser_t *body = mpc_new("body");
	mpc_parser_t *stmt_list = mpc_new("stmt_list");
	mpc_parser_t *stmt = mpc_new("stmt");
	mpc_parser_t *loop = mpc_new("loop");
	mpc_parser_t *assignm = mpc_new("assign");
	mpc_parser_t *cond = mpc_new("cond");
	mpc_parser_t *logics = mpc_new("logics");
	mpc_parser_t *comps = mpc_new("comps");
	mpc_parser_t *sums = mpc_new("sums");
	mpc_parser_t *prods = mpc_new("prods");
	mpc_parser_t *rdiv = mpc_new("rdiv");
	mpc_parser_t *value = mpc_new("value");
	mpc_parser_t *id = mpc_new("id");
	mpc_parser_t *message = mpc_new("message");

	mpca_lang(MPCA_LANG_DEFAULT,
"stmt		: <loop>																	"
"			| <body>																	"
"			| <assign>																	"
"			| <cond>																	"
"			| <logics>																	"
"			;																			"
"loop		: '[' <stmt> \"]->\" <stmt>													"
"			| <body> \"<-[\" <stmt> ']'													"
"			;																			"
"body		: '{' <stmt_list> '}'														"
"			;																			"
"stmt_list	: (<stmt> ';')*																"
"			;																			"
"assign		: <id> '=' <stmt>															"
"			;																			"
"cond		: <logics> '?' <stmt> (':' <stmt>)?											"
"			;																			"
"logics		: <comps> ((\"||\" | \"&&\") <logics>)?										"
"			;																			"
"comps		: <sums> ((\"==\" | \"!=\" | \">=\" | \"<=\" | '<' | '>') <comps> )?		"
"			;																			"
"sums		: <prods> (('+' | '-') <sums>)?												"
"			;																			"
"prods		: <rdiv> (('*' | '/') <prods>)?												"
"			;																			"
"rdiv		: <value> ('%' <rdiv>)?														"
"			;																			"
"value		: '-' <value>																"
"			| '!' <value>																"
"			| '#' <value>																"
"			| '$' <value>																"
"			| '(' <stmt> ')'															"
"			| <id>																		"
"			| /[0-9]+/																	"
"			| '@'																		"
"			| <message>																	"
"			;																			"
"id			: /[a-zA-Z_][a-zA-Z0-9_]*/;													"
"message	: /\"(\\\\.|[^\"])*\"/ | /\'(\\\\.|[^\'])*\'/;								"
		, stmt, loop, body, stmt_list, assignm, cond, logics, comps, sums, prods, rdiv, value, id, message, NULL);


	mpc_result_t r;

	// Last param of the list will have a NULL name.
	struct param *paramlist = malloc(sizeof(struct param));
	paramlist[0].name = NULL;

	if(mpc_parse_contents(filename, stmt_list, &r)) {
		int ret;
		paramlist = process_stmt_list(r.output, paramlist, &ret);
		mpc_ast_delete(r.output);
	} else {
		mpc_err_print(r.error);
		mpc_err_delete(r.error);
	}

	free(paramlist);

	mpc_cleanup(12, body, stmt_list, stmt, assignm, cond, logics, comps, sums, prods, rdiv, value, id, message);

	return 0;
}
